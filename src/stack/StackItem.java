package stack;

public class StackItem {
    Object item;
    private StackItem next;

    public StackItem getNext() {
        return next;
    }

    public void setNext(StackItem next) {
        this.next = next;
    }

    public StackItem(Object item){
        this.item = item;
    }

}
