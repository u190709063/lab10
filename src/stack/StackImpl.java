package stack;

public class StackImpl implements Stack{
    StackItem top;
    /*public StackImpl(){
        top = null;
    }*/
    @Override
    public void push(Object item) {
        if( empty() )
        {
            top = new StackItem(item);
        }
        else
        {
            StackItem newitem = new StackItem(item);
            newitem.setNext(top);
            top = newitem;
        }
    }

    @Override
    public Object pop() {
        if (empty()) {
            return null;
        }else if (top.getNext()==null){
            Object item = top.item;
            top = null;
            return item;
        }else{
            StackItem poppeditem = top;
            top = poppeditem.getNext();
            return poppeditem.item;
        }
    }
    @Override
    public boolean empty() {
        return top == null;
    }
}
